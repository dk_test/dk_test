<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190923122124 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE log (id INT AUTO_INCREMENT NOT NULL, log_text VARCHAR(1500) NOT NULL, log_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_stats (id INT AUTO_INCREMENT NOT NULL, provider_name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, cnt INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smslog (id INT AUTO_INCREMENT NOT NULL, reciver_number VARCHAR(255) NOT NULL, api_gateway VARCHAR(255) NOT NULL, sms_text VARCHAR(255) NOT NULL, status TINYINT(1) NOT NULL, send_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql("INSERT INTO `provider_stats` (`provider_name`, `status`, `cnt`) VALUES ('sms1', '1', 0)");
        $this->addSql("INSERT INTO `provider_stats` (`provider_name`, `status`, `cnt`) VALUES ('sms1', '0', 0)");
        $this->addSql("INSERT INTO `provider_stats` (`provider_name`, `status`, `cnt`) VALUES ('sms2', '1', 0)");
        $this->addSql("INSERT INTO `provider_stats` (`provider_name`, `status`, `cnt`) VALUES ('sms2', '0', 0)");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE log');
        $this->addSql('DROP TABLE provider_stats');
        $this->addSql('DROP TABLE smslog');
    }
}
