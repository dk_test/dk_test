<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SmsLogRepository")
 */
class Smslog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reciver_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $api_gateway;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sms_text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $send_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReciverNumber(): ?string
    {
        return $this->reciver_number;
    }

    public function setReciverNumber(string $reciver_number): self
    {
        $this->reciver_number = $reciver_number;

        return $this;
    }

    public function getSmsText(): ?string
    {
        return $this->sms_text;
    }

    public function setSmsText(string $sms_text): self
    {
        $this->sms_text = $sms_text;

        return $this;
    }

    public function getApiGateway(): ?string
    {
        return $this->api_gateway;
    }

    public function setApiGateway(string $api_gateway): self
    {
        $this->api_gateway = $api_gateway;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSendDate(): ?\DateTimeInterface
    {
        return $this->send_date;
    }

    public function setSendDate(\DateTimeInterface $send_date): self
    {
        $this->send_date = new $send_date;

        return $this;
    }


}
