<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogRepository")
 */
class Log
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1500)
     */
    private $log_text;

    /**
     * @ORM\Column(type="date")
     */
    private $log_date;

    public function __toString()
    {
        try {
            return '{"log_text":"' . $this->getLogText() . '","log_date": "' . $this->getLogDate()->format('Y-m-d') . '"}';
        } catch (\Exception $e) {
            return "error";
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogText(): ?string
    {
        return $this->log_text;
    }

    public function setLogText(string $log_text): self
    {
        $this->log_text = $log_text;

        return $this;
    }

    public function getLogDate(): ?\DateTimeInterface
    {
        return $this->log_date;
    }

    public function setLogDate(\DateTimeInterface $log_date): self
    {
        $this->log_date = $log_date;

        return $this;
    }

}
