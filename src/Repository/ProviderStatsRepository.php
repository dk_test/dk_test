<?php

namespace App\Repository;

use App\Entity\ProviderStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProviderStats|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderStats|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderStats[]    findAll()
 * @method ProviderStats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderStatsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderStats::class);
    }

    public function findByProviderStatus($provider,$status)
    {

        return  $this->createQueryBuilder('p')
            ->andWhere('p.provider_name = :provider_name')
            ->setParameter('provider_name', $provider)
            ->andWhere('p.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getOneOrNullResult()
            ;

    }

    // /**
    //  * @return ProviderStats[] Returns an array of ProviderStats objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderStats
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    function getRatioLog(){
        return $this->createQueryBuilder('sms')
            ->select('sms.provider_name , sms.status, sms.cnt')
            ->getQuery()
            ->getResult();
    }
}
