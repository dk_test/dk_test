<?php

namespace App\Repository;


use App\Entity\Smslog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Smslog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Smslog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Smslog[]    findAll()
 * @method Smslog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmsLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Smslog::class);
    }


    /**
     * To count all sent sms groupby provider
     *
     * @return array
     */
    public function countAll()
    {
        return $this->createQueryBuilder('l')
            ->select('COUNT(l)')
            ->andWhere('l.status = :val')
            ->setParameter('val', 1)
            ->getQuery()
            ->getResult();
    }


/*
* To count all sent sms groupby provider
*
* @return array
*/
    public function countForProviders()
    {
        return $this->createQueryBuilder('sms')
            ->select('COUNT(sms) as cnt,sms.api_gateway')
            ->groupby('sms.api_gateway')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return ProviderStats[] Returns an array of ProviderStats objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderStats
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function mostTenMessageReceivers()
    {
        return $this->createQueryBuilder('sms')
            ->select('sms.reciver_number AS number, COUNT(sms) as cnt')
            ->groupby('number')
            ->setMaxResults(10)
            ->orderBy('cnt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
