<?php

namespace App\Controller;

use App\Repository\ProviderStatsRepository;
use App\Repository\LogRepository;
use App\Repository\SmsLogRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReportController extends AbstractController
{
    /**
     * @Route("/reports", methods={"GET"}, name="report")
     * @param SmsLogRepository $smslogRepository
     */
    public function index(SmsLogRepository $smslogRepository,ProviderStatsRepository $providerStatsRepository)
    {
        $s=[
            'sms1'=>[
                'failed'=>0,
                'sum'=>0
            ],
            'sms2'=>[
                'failed'=>0,
                'sum'=>0
            ]
        ];

        $stats=($providerStatsRepository->getRatioLog());
        foreach ($stats as $stat){
            if($stat['status']==0)
                $s[$stat['provider_name']]['failed']=$stat['cnt'];
            $s[$stat['provider_name']]['sum']+=$stat['cnt'];
        }


        return $this->render('report/report.html.twig', [
            'countAllSent' => $smslogRepository->count(array()),
            'countAllSuccSent' => $smslogRepository->count(array('status'=>1)),
            'eachProviderUsage' => $smslogRepository->countForProviders(),
            'providersLog' => $s,
            'mostTenMessageReceivers' => $smslogRepository->mostTenMessageReceivers(),
        ]);
    }
}