<?php

namespace App\Controller;

use App\Entity\ProviderStats;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Log;
use App\Entity\Smslog;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Controller\ParamFetcherInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MainController extends AbstractController
{
    private $err = [];
    private $data = [];
    private $baseMockService = "http://127.0.0.1:8081";
    private $smsService1 = "/sms1";
    private $smsService2 = "/sms2";
    private $response = "";

    /**
     * @Route("/send", methods={"GET"}, name="send")
     * @param Request $request
     * @return Response
     */

    function send(Request $request)
    {
        $validator = Validation::createValidator();

        $constraint = new Assert\Collection([
            'number' => [
                new Assert\Regex("/^(?:98|\+98|0098|0)?9[0-9]{9}$/"),
                new Assert\NotBlank()
            ],
            'body' => [
                new Assert\Regex("/^\w+/"),
                new Assert\NotBlank()
            ]
        ]);

        $validationResult = $validator->validate(
            [
                'number' => $request->request->get('number'),
                'body' => $request->request->get('body'),
            ],
            $constraint
        );

        foreach ($validationResult as $res) {
            $this->err[] = $res->getMessage();
        }
        if (sizeof($this->err)) {
            return $this->json($this->returnResponse(500, "Validation Error"));
        }
        $this->data['number'] = $request->request->get('number');
        $this->data['body'] = $request->request->get('body');
        try {
            $provider = (rand(1,20)>10)?$this->smsService1:$this->smsService2;
            $this->SendSMS($provider, $this->data);
        } catch (Exception $e) {
            return $this->json($this->returnResponse(500, $e->getMessage()), 500);
        }

        return $this->json($this->returnResponse(200, "Message Sent"), 200);

    }

    /**
     * @param $status
     * @param $message
     * @return mixed
     */
    private function returnResponse($status, $message)
    {
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $this->data;
        $response['error'] = $this->err;
        return $response;
    }

    /**
     * @param $url
     * @param $params
     * @return void
     * @throws Exception
     */
    private function SendSMS($url, $params)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => $this->baseMockService
        ]);

        try {
            $response = $client->request('POST', $url, [
                'debug' => false,
                'timeout' => 2,
                'on_stats' => function (\GuzzleHttp\TransferStats $stats) {
                    $transferTimeout = $stats->getTransferTime();
                },
                'form_params' => $params
            ]);

            $this->logSMS($params['number'], $params['body'], 1, $url);
        } catch (GuzzleException $e) {
            $this->logSMS($params['number'], $params['body'], 0, $url);
            throw new Exception($e->getCode());
        }
    }

    /**
     * @param $reciver
     * @param $text
     * @param $status
     * @param $gateway
     * @return Response
     */
    private function logSMS($reciver, $text, $status, $gateway)
    {
        $sms = new Smslog();
        $ps = new ProviderStats();
        $gateway = ltrim($gateway,'/');
        $entityManager = $this->getDoctrine()->getManager();

        $rep = $entityManager->getRepository(ProviderStats::class);

        $res= $rep->findByProviderStatus($gateway,$status);
        $res->setCnt($res->getCnt()+1);
        $entityManager->persist($res);
        $entityManager->flush();





        try {
            $sms->setReciverNumber($reciver);
            $sms->setSmsText($text);
            $sms->setStatus($status);
            $sms->setApiGateway($gateway);
            $sms->setSendDate(new \DateTime());

            $entityManager->persist($sms);
            $entityManager->flush();
        } catch (\Exception $e) {
            echo $e->getMessage();
            return Response::create("error", 500);
        }
    }
}
